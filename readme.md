## Book Source Code

This repository contains sample code that is supplied with [Writing APIs With Lumen](https://leanpub.com/lumen-apis). Thank you for purchasing my book!

If you haven't purchased my book yet, you should consider [learning more](https://leanpub.com/lumen-apis) and downloading a sample. The book provides code explanations and development tips, and illustrates a specific development workflow that is absent from the repository.

## Still Have Questions?

If you are still not sure about purchasing my book, or have questions, reach out to me at __paulrredmond@gmail.com__ and see what promotions I might be running or explain your situation and I'll consider it :)

Copyright (C) 2016. Paul Redmond.
